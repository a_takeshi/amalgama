﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextBoxSystem : MonoBehaviour {
    /// <summary>
    /// Controla o sistema de texto da caixa de dialogo
    /// chara é a variável que guarda o personagem que fornece o texto do diálogo e
    /// observação.
    /// </summary>
    public GameObject chara;
    public GameObject player;
    public string tipo;
    public string [] fala;
    private float typeSpeed = 0.03f;
    private float commaPause = 0.2f;
    private float dotPause = 0.3f;
    public Text txt;
    public AudioSource sound1;

    public int index = 0;

    private Camera mainCamera;
    private bool skipped = false;
    public Coroutine coroutine;

    void Start ()
    {
        choose(tipo);
        mainCamera = Camera.main;
        mainCamera.GetComponent<CameraBehavior>().enabled = false;
        player = GameObject.FindGameObjectWithTag("Player");
        txt = gameObject.GetComponent<Text>();
        txt.text = "";
        coroutine = StartCoroutine(TypeWriter(fala[index], txt));
        player.SetActive(false);
        chara.SetActive(false);
    }

    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (skipped == false)
            {
                StopCoroutine(coroutine);
                txt.text = fala[index];
                skipped = true;
            }
            else
            {
                if (index >= fala.Length - 1)
                {
                    endThisEvent();
                }
                else
                {
                    txt.text = "";
                    index++;
                    coroutine = StartCoroutine(TypeWriter(fala[index], txt));
                    skipped = false;
                }
            }
        }
    }

    void endThisEvent ()
    {
        StopCoroutine(coroutine);
		if (chara) {
			chara.SetActive(true);
		}
        player.SetActive(true);
        mainCamera.GetComponent<CameraBehavior>().enabled = true;
        Destroy(transform.root.gameObject);
    }

    IEnumerator TypeWriter(string source, Text target)
    {
        for (int i = 0; i < source.Length; i++)
        {
            target.text = source.Substring(0, i);
			if (sound1) {
				sound1.Play();
			}

            if (source[i] == ((",").ToCharArray())[0])
            {
                yield return new WaitForSeconds(commaPause);
            }

            if(source[i] == ((".").ToCharArray())[0])
            {
                yield return new WaitForSeconds(dotPause);
            }

            if (source[i] == ((":").ToCharArray())[0])
            {
                yield return new WaitForSeconds(dotPause);
            }

            if (source[i] == (("!").ToCharArray())[0])
            {
                yield return null;
            }

            if (source[i] == (("\t").ToCharArray())[0])
            {
                yield return null;
            }

            yield return new WaitForSeconds(typeSpeed);
        }
        skipped = true;
    }

    public void choose(string x)
    {
        if (x == "Observar")
        {
            fala = chara.GetComponent<CharacterText>().observar;
        }
        if (x == "Dialogo")
        {
            fala = chara.GetComponent<CharacterText>().dialogo;
        }
    }

    public void setCharacter (GameObject n)
    {
        // Qual personagem a função pega o texto
        chara = n;
        return;
    }
}
