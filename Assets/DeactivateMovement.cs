﻿using UnityEngine;
using System.Collections;

public class DeactivateMovement : MonoBehaviour {

	[SerializeField]
	private PlayerMovement playerMovement;
	[SerializeField]
	public bool isActive;


	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftControl)) {
			if (isActive == true) {
				playerMovement.enabled = false;
				isActive = false;
			} else {
				playerMovement.enabled = true;
				isActive = true;
			}
		}
	}
}
