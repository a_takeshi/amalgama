﻿using UnityEngine;
using System.Collections;

public class CameraCharacterSwitch : MonoBehaviour {

	[SerializeField]
	private Transform[] personagens;
	[SerializeField]
	private CameraBehavior cameraBehavior;
	private int index;

	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftControl)) {
			index++;
			if (index >= personagens.Length) {
				index = 0;
				cameraBehavior.target = personagens [index];
			} 
			else {
				cameraBehavior.target = personagens [index];
			}

		}
	}
}
