﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
public class Atrract : MonoBehaviour {

	[SerializeField]
	private Transform target;
	[SerializeField]
	public bool attracting = false;
	[SerializeField]
	private Rigidbody2D rigidBody;
	[SerializeField]
	private float speed;
	public CameraEffects camera;
	public Image image;
	public float fadeSpeed = 0.05f;
	private Color fadeWhite = new Color(255,255,255,255);
	private float attractionDelay = 0.5f;
	private int current;
	public bool deactivated;
	public AudioSource fusion;
	private bool atracted;

	void Start () {
		current = SceneManager.GetActiveScene().buildIndex;
		deactivated = GetComponent< DeactivateMovement > ().isActive;

	}

	void LoadNext () {
		Application.LoadLevel (current + 1);
	}


	void Update () {
		if (atracted == false || attracting == false) {

			if (Input.GetKeyDown (KeyCode.Space)) {
				atracted = true;
				GetComponent <  DeactivateMovement > ().enabled = false;
				attracting = true;	
				GetComponent < PlayerMovement > ().enabled = false;
				if (transform.position.x > target.position.x) {
					GetComponent < PlayerMovement > ().facingRight = false;
					target.localScale = new Vector3 (1, 1, 1);
				} 
				if (camera) {
					camera.GetComponent<CameraCharacterSwitch> ().enabled = false;
				} else {
					GetComponent < PlayerMovement > ().facingRight = true;
					target.localScale = new Vector3 (1, 1, -1);
				}

				if (fusion) {
					fusion.Play ();
				}
			}
		}
	}

	void FixedUpdate () {
		if (attracting == true) {
			rigidBody.constraints = RigidbodyConstraints2D.FreezeAll;
			transform.position = Vector2.Lerp (transform.position, new Vector2 (transform.position.x, 
												transform.position.y + 0.09f), 0.1f );
			
			if (camera) {
				camera.StartCoroutine ("CameraShake");
			}
			Invoke ("Grudar", attractionDelay);
		} else {
			
			rigidBody.constraints = RigidbodyConstraints2D.None;
			rigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;

			if (camera) {
				camera.StopCoroutine ("CameraShake");
			}
		}
	}

	void Grudar () {
		transform.position = Vector2.Lerp (transform.position, Vector2.ClampMagnitude(target.position, 5f), speed * Time.deltaTime);
	}

	void OnTriggerEnter2D (Collider2D collision) {	
		
		if (collision.gameObject.tag == "Wall" || collision.gameObject.tag == "Ob" ) {
			GetComponent <  DeactivateMovement > ().enabled = true;
			if (atracted) {
				GetComponent < PlayerMovement > ().enabled = true;  
				atracted = false;
			}

				
			attracting = false;
			rigidBody.velocity = new Vector2(0,0);
			if (camera) {
				camera.StopCoroutine ("CameraShake");	
				camera.GetComponent <CameraCharacterSwitch> ().enabled = true;
			}	
		}
		if (collision.gameObject.tag == "Player") {
			if (image && attracting == true) {
				image.color = Color.LerpUnclamped (image.color, fadeWhite, fadeSpeed * Time.deltaTime);
				Invoke ("LoadNext", 2);
				fusion.volume = Mathf.Lerp (fusion.volume, 0, 0.5f * Time.deltaTime);
			}
		}
	}

}
