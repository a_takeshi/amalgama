﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {
	public AudioSource click;

	public void Load( int level) {
		click.Play ();
		Application.LoadLevel (level);
	}

	public void Exit (){
		Application.Quit ();
	}

}
