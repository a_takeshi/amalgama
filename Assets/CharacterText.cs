﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterText : MonoBehaviour {

    // Prepara o texto bruto para ser impresso na caixa de diálogo

    public TextAsset [] textoDialogo;
    public TextAsset [] textoObservar;

    public string [] dialogo;
    public string [] observar;

    public int index = 0;

    void Start()
    {
        dialogo = cutText(textoDialogo[0]);
        observar = cutText(textoObservar[0]);
    }

    string [] cutText (TextAsset x)
    {
        string [] y = x.ToString().Split('\n');
        return y;
    }

    void updateDialogue ()
    {
        switch(index)
        {
            case (0):
                cutText(textoDialogo[0]);
                break;

            default:
                break;
        }
    }
}
