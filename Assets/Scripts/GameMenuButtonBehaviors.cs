﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameMenuButtonBehaviors : MonoBehaviour {

    public void showWindow(RectTransform panel)
    {
        panel.localPosition = new Vector2(0, 0);
    }

    public void hideButton(RectTransform thisButton)
    {
        thisButton.localPosition = new Vector2(-5000, 0);
    }

    public void showTextBox(GameObject textBox)
    {
        GameObject thirdMenu = Instantiate(textBox);
        thirdMenu.name = "Caixa de Texto";
        // n é o Personagem que tem o texto.
        GameObject n = this.transform.root.gameObject;
        n.GetComponent<CharacterClickInteraction>().open = false;
        TextBoxSystem x = thirdMenu.GetComponentInChildren<TextBoxSystem>();
        x.setCharacter(n);
        // Pega a lista de sprites do personagem (para dialogo) e transfere para uma lista no boxe de dialogo.
        thirdMenu.GetComponent<DialogueSpriteControler>().sprites = n.GetComponent<CharacterSprites>().sprites;
        Destroy(this.transform.parent.gameObject);
    }
}
