﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Controles Gerais/Sair do Jogo")]
public class GameQuiter : MonoBehaviour {

	void Update () {
       
        // Sair do jogo 
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

    }
}
