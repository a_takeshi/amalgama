﻿using UnityEngine;
using System.Collections;

public class DeactivateClickMovement : MonoBehaviour {
    private ClickMovement clickMove;

    void Start ()
    {
        clickMove = GameObject.FindGameObjectWithTag("Player").GetComponent<ClickMovement>();
    }

    void OnMouseOver ()
    {
        clickMove.enabled = false;
    }

    void OnMouseExit ()
    {
        clickMove.enabled = true;
    }

    void OnDestroy ()
    {
        clickMove.enabled = true;
    }
}
