﻿using UnityEngine;
using System.Collections;

public class CameraEffects : MonoBehaviour {

    public CameraBehavior cameraBehavior;

    [SerializeField]
    private float shakeInterval;
    [SerializeField]
    private int shakeQuantity;
    private float shakeDistanceX;
    private float shakeDistanceY;
    private Vector2 defaultPos;
	public Coroutine shake;

	[SerializeField]
	private float fadeSpeed;

    [SerializeField]
    private float min;

    [SerializeField]
    private float max;

    IEnumerator CameraShake () {
        defaultPos = transform.position;

        for (int i = 0; i < shakeQuantity; i++)
        {
            float value = Random.Range(0, 1f);
            if(value > 0.5f)
            {
                transform.position = new Vector2(transform.position.x + shakeDistanceX, transform.position.y + shakeDistanceY);
            }
            else {
                transform.position = new Vector2(transform.position.x + shakeDistanceX, transform.position.y + shakeDistanceY);
            }

            shakeDistanceX = Random.Range(min, max);
            shakeDistanceY = Random.Range(min, max);

            yield return new WaitForSeconds(shakeInterval);
        }
        transform.position = defaultPos;
    }
}
