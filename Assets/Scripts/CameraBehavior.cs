﻿using UnityEngine;
using System.Collections;

public class CameraBehavior : MonoBehaviour {

    //FollowPlayer
    public Transform target;
    private Camera thisCamera;
    [SerializeField]
    private float minCam = 0.51f;
    [SerializeField]
    private float maxCam = 2.92f;
    [SerializeField]
    private float maxSpeed = 0.2f;
    [SerializeField]
    private float offsetY;

    //Scroll
    private float newScrollPos;
    private float current = 2.22f;
    private float scroll = 0.52f;
    private float smoothVelocity;
    private float scrollSmoothT = 0.19f;

    public Vector2 position = new Vector2(0,0);

    void Awake() {
        thisCamera = gameObject.GetComponent<Camera>();
    }

    void FixedUpdate () {
        // Camera segue o jogador
        if(target)
        {
            transform.position = Vector2.SmoothDamp(transform.position, new Vector2(target.position.x, target.position.y - offsetY), ref position, maxSpeed);

        }
       
        // Scroll zoom
        if (Input.GetAxis("Mouse ScrollWheel") > 0 )
        {   
            // Zoom out
            if(thisCamera.orthographicSize > minCam)
            {
                current = thisCamera.orthographicSize - scroll;
            }
        }
            // Zoom in
        if(Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (thisCamera.orthographicSize < maxCam)
            {
                current = thisCamera.orthographicSize + scroll; 
            }
        }

        newScrollPos = Mathf.SmoothDamp(thisCamera.orthographicSize, current, ref smoothVelocity, scrollSmoothT);
        thisCamera.orthographicSize = newScrollPos;
	}
}
