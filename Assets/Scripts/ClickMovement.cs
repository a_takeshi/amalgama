﻿using UnityEngine;
using System.Collections;

public class ClickMovement : MonoBehaviour {

    public Vector2 clickPos;
    public Vector2 newPos;
    public Camera cameraM;
    private float speed;
    private bool isRunning;
    private bool move;

    void Start()
    {
        speed = gameObject.GetComponent<PlayerMovement>().speed;
        isRunning = gameObject.GetComponent<PlayerMovement>().isRunning;
        newPos = transform.position;
    }

    void Update() {

        clickPos = cameraM.ScreenToWorldPoint(Input.mousePosition);

        // Quando o botão esquerdo do mouse é clicado, pegamos a posição do mouse e guardamos na variável newPos;
        if ( Input.GetMouseButtonDown(0) )
        {
            newPos = clickPos;
            move = true;
            if ( clickPos.x > transform.position.x )
            {
                gameObject.GetComponent<PlayerMovement>().facingRight = true;
            } else
            {
                gameObject.GetComponent<PlayerMovement>().facingRight = false;
            }
        }
    }

    void FixedUpdate ()
    {
        if (move == true)
        {
            // Quando move for verdadeiro, o objeto é interpolado para o destino ( newPos ).
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(newPos.x, transform.position.y), speed * Time.deltaTime);
            isRunning = true;
            if (transform.position.x == newPos.x)
            {
                move = false;
            }

            // Caso o jogador clique para andar para esquerda ou direita, o movimento é interrompido.
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
            {
                move = false;
            }
        }
    }
}
