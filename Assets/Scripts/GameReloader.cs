﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[AddComponentMenu("Controles Gerais/Reload")]
public class GameReloader : MonoBehaviour {

    public int currentLevel;

    void Start ()
    {
        currentLevel = SceneManager.GetActiveScene().buildIndex;
    }

	void Update () {
	    if(Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(currentLevel);
            Destroy(GameObject.FindGameObjectWithTag("Player") );
        }
	}
}
