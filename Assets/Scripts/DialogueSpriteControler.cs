﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogueSpriteControler : MonoBehaviour {

    // Controla as imagens dos personagens durante o diálogo.
    public TextBoxSystem text;
    public int index;
    public Sprite[] sprites;
    public GameObject image;
    public RawImage RImage;

    void Start()
    {
        // Tudo isso configura a imagem que é colocada na tela.
        // Tanto tamanho, posição e localização das anchors.
        image = new GameObject();
        image.transform.SetParent(transform);
        image.transform.SetAsFirstSibling();
        image.name = "Avatar";
        image.AddComponent<RawImage>();
        RImage = image.GetComponent<RawImage>();
        RImage.texture = sprites[index].texture;
        RImage.GetComponent<RectTransform>().anchorMax = new Vector2(0.79375f, 0f);
        RImage.GetComponent<RectTransform>().anchorMin = new Vector2(0.79375f, 0f);
        RImage.GetComponent<RectTransform>().anchoredPosition3D = new Vector2(-4.7f, 250f);
        RImage.GetComponent<RectTransform>().sizeDelta = new Vector2(247, 367); 
    }   

    void Update ()
    {
        index = text.index;
        if(index == 4)
        {
            RImage.texture = sprites[1].texture;
        }
    }

}
