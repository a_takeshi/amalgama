﻿using UnityEngine;
using System.Collections;

//Adiciona o script no menu superior do editor
[AddComponentMenu("Movimentação/Plataforma")]
public class PlayerMovement : MonoBehaviour {

    private Rigidbody2D rbody;
    private Animator animator;
    public float speed = 2.5f, jumpForce = 4.0f;
    public bool facingRight;
    public bool isFloor;
    public bool isRunning;
    public bool canJump;
    public bool jump;
	public AudioSource pulo;

    public Transform A;
    public Transform B;

    // Movimentação
    private bool andarD = false;
    private bool andarL = false;

    void Start() {
        animator = gameObject.GetComponent<Animator>();
        rbody = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update() {
        Debug.DrawLine(A.position, B.position, Color.red);
        Raycast();
        animator.SetBool("isFloor", isFloor);
        animator.SetBool("facingRight", facingRight);
        animator.SetBool("isRunning", isRunning);

        if ((Input.GetKeyDown(KeyCode.W)) && (canJump == true) )
        {
            jump = true;
        } else
        {
            jump = false;
        }

        if (Input.GetKey(KeyCode.D))
        {
            andarD = true;
            facingRight = true;
            isRunning = true;
        }
        else
        {
            andarD = false;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            isRunning = false;
        }

        if (Input.GetKey(KeyCode.A))
        {
            andarL = true;
            facingRight = false;
            isRunning = true;
        }
        else
        {
            andarL = false;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            isRunning = false;
        }

		if (facingRight == true) {
			transform.localScale = new Vector3 (-1, 1, 1);
		} else {
			transform.localScale = new Vector3 (1, 1, 1);
		}
    }

    void FixedUpdate()
    {
        if (andarD)
        {
            transform.position = new Vector2((transform.position.x) + (speed * Time.deltaTime), transform.position.y);
        }

        if (andarL)
        {
            transform.position = new Vector2((transform.position.x) + (-speed * Time.deltaTime), transform.position.y);
        }
        if (jump)
        {
            rbody.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
			jump = false;
			if (pulo) {
				pulo.pitch += Random.Range(-0.1f,0.1f);
				pulo.Play ();
			}
        }
    }

    void Raycast () {
        Debug.DrawLine(A.position, B.position, Color.red);
        RaycastHit2D hit = Physics2D.Linecast(A.position, B.position);

        if (Physics2D.Linecast(A.position, B.position))
        {
			if (hit.collider.gameObject.tag == "Floor" || hit.collider.gameObject.tag == "Ob" )
            {
                isFloor = canJump = true;
            }
            else
            {
                isFloor = canJump = false;
            }
        }
        else {
            isFloor = canJump = false;
        }
    }
}