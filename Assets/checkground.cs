﻿using UnityEngine;
using System.Collections;

public class checkground : MonoBehaviour {
	public Transform A;
	public Transform B;
	public checkground check;
	public bool open;
	public Animator animator;
	public bool isFloor;
	// Use this for initialization

	// Update is called once per frame
	void Update () {
		animator.SetBool ("isFloor", isFloor);
		Raycast ();
		if (Input.GetKeyDown (KeyCode.LeftControl)) {
			
			if (open == true) {
				check.enabled = false;
				open = false;
			} 
			else {
				check.enabled = true;
				open = true;
			}
		}


	}

	void Raycast () {
		Debug.DrawLine(A.position, B.position, Color.red);
		RaycastHit2D hit = Physics2D.Linecast(A.position, B.position);

		if (Physics2D.Linecast(A.position, B.position))
		{
			if (hit.collider.gameObject.tag == "Floor" || hit.collider.gameObject.tag == "Ob" )
			{
				isFloor = true;
			}
			else
			{
				isFloor = false;
			}
		}
		else {
			isFloor = false;
		}
	}

}
