﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour {
	public bool lever = false;
	public Transform wall;
	public Transform walltarget;
	public float speed = 5f;
	public bool move = false;
	private Vector2 walldefault;
	private bool onLever = false;


	// Use this for initialization
	void Start () {
		walldefault = wall.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (onLever == true) {
			if (Input.GetKeyDown (KeyCode.E)) {

				if (lever == false) {
					transform.localScale = new Vector2 (-0.09f, 0.09f);
					lever = true;
					move = true;
				} 
				else {
					transform.localScale = new Vector2 (0.09f, 0.09f);
					lever = false;
					move = false;
				}

			}
		}

		
	}
	void FixedUpdate () {
		if (move == true) {

			wall.transform.position = Vector2.MoveTowards (wall.transform.position, walltarget.position, speed * Time.deltaTime);
		} else {
			
			wall.transform.position = Vector2.MoveTowards (wall.transform.position, walldefault, speed * Time.deltaTime);

		}
	}

	void OnTriggerEnter2D (Collider2D collision){

		if (collision.transform.tag == "Player") {
			onLever = true;
		}
			
	}

	void OnTriggerExit2D (Collider2D collision) {
		onLever = false;
	}
	 




	}


