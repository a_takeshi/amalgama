﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterClickInteraction : MonoBehaviour {
    // Painel original
    public GameObject panel;
    public Camera mainCamera;

    public CharacterText text;

    // Painel clone
    public GameObject panel1;
    public Vector2 mousePosition;
    public bool open;
    private Vector2 offsetX = new Vector2(0.5f, 0.3f);
    public SpriteRenderer render;
	public Sprite hoverSprite;
	private Sprite defaultSprite;

    // Som de abrir menu
    public AudioSource beepOpen;
    public AudioSource beepClose;

    void Awake ()
    {
        render = GetComponent<SpriteRenderer>();
        text = gameObject.GetComponent<CharacterText>();
        mainCamera = Camera.main;
    }

    void Update() {
        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
    }

	void OnMouseEnter () {
		defaultSprite = GetComponent<SpriteRenderer> ().sprite;
		GetComponent<SpriteRenderer> ().sprite = hoverSprite;
	}

	void OnMouseExit () {
		GetComponent<SpriteRenderer> ().sprite = defaultSprite;
	}
		
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (open)
            {
                open = false;
                beepClose.Play();
                Destroy(panel1);
            } else
            {
                open = true;
                beepOpen.Play();
                panel1 = Instantiate(panel, mousePosition + offsetX, transform.rotation) as GameObject;
				panel1.transform.SetParent(gameObject.transform);       
            }
        }
    }
}
